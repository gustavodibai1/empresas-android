package com.example.empresasandroid.model.retrofit;

import com.example.empresasandroid.model.entity.Enterprise;
import com.example.empresasandroid.model.entity.ResponseEnterprise;
import com.example.empresasandroid.model.entity.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class RetrofitInstance {

    public static String BASE_URL = "http://empresas.ioasys.com.br/api/v1/";

    public AccessEndPoints getAPI() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        OkHttpClient.Builder client = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS);

        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client.build())
                .build();

        return retrofit.create(AccessEndPoints.class);
    }

    public interface AccessEndPoints {

        @POST("users/auth/sign_in")
        @Headers("Content-Type: application/json")
        Call<User> loginUsuario(@Body User usuario);

        @GET("enterprises/")
        Call<JsonObject> getEmpresas(@Header("access-token") String token,
                                     @Header("client") String client,
                                     @Header("uid") String uid);

        @GET("enterprises/{id}")
        Call<ResponseEnterprise> show(@Path("id") int id);

    }
}