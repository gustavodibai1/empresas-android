package com.example.empresasandroid.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Enterprise {
    private float id;
    private String enterprise_name;
    private String description;
    private String photo;
    private String country;
    private EnterpriseTypeModel enterprise_type;

    public Enterprise(String enterprise_name, String description, String photo, String country, EnterpriseTypeModel enterprise_type)
    {
        this.enterprise_name = enterprise_name;
        this.description = description;
        this.photo = photo;
        this.country = country;
        this.enterprise_type = enterprise_type;
    }

    public float getId() {
        return id;
    }

    public void setId(float id) {
        this.id = id;
    }

    public String getEnterprise_name() {
        return enterprise_name;
    }

    public void setEnterprise_name(String enterprise_name) {
        this.enterprise_name = enterprise_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }


    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public EnterpriseTypeModel getEnterprise_type() {
        return enterprise_type;
    }

    public void setEnterprise_type(EnterpriseTypeModel enterprise_type) {
        this.enterprise_type = enterprise_type;
    }
}
