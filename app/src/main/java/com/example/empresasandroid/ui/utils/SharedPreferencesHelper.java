package com.example.empresasandroid.ui.utils;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class SharedPreferencesHelper {
    private Context context;

    public SharedPreferencesHelper(Context context) {
        this.context = context;
    }

    public void saveToSharedPreferences(String token, String client, String uid) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("accessData", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("TOKEN", token);
        editor.putString("CLIENT", client);
        editor.putString("UID", uid);
        editor.apply();
    }

    public String getToken(){
        SharedPreferences sharedPreferences = context.getSharedPreferences("accessData", MODE_PRIVATE);
        return sharedPreferences.getString("TOKEN", "");
    }

    public String getClient(){
        SharedPreferences sharedPreferences = context.getSharedPreferences("accessData", MODE_PRIVATE);
        return sharedPreferences.getString("CLIENT", "");
    }

    public String getUID(){
        SharedPreferences sharedPreferences = context.getSharedPreferences("accessData", MODE_PRIVATE);
        return sharedPreferences.getString("UID", "");
    }
}
