package com.example.empresasandroid.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.empresasandroid.R;
import com.example.empresasandroid.model.entity.Enterprise;

import java.util.ArrayList;
import java.util.List;



//Adapter serve para pegar as informacoes adaptar e encaixar no RecyclerView
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> implements Filterable {

    private Context context;
    List<Enterprise> enterprises;
    Enterprise empresa;
    private boolean enterpriseListTemp = false;
    private static List<Enterprise> enterpriseList;
    private static List<Enterprise> enterpriseListFull;

    public RecyclerAdapter(Context context, List<Enterprise> enterprises) {
        this.context = context;
        this.enterprises = enterprises;
    }

    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerAdapter.ViewHolder viewHolder, int i) {
        final Enterprise enterprise = enterprises.get(i);

        //Carrega elemento no layout
        Glide.with(context)
                .asBitmap()
                .load(context.getString(R.string.BASE_URL) + enterpriseList.get(i).getPhoto())
                .into(viewHolder.img);

        viewHolder.txtName.setText(enterprise.getEnterprise_name());
        viewHolder.txtCountry.setText(enterprise.getCountry());
        viewHolder.txtType.setText(enterprise.getEnterprise_type().getEnterprise_type_name());

    }

    @Override
    public int getItemCount() {
        return enterprises.size();
    }

    @Override
    public Filter getFilter()
    {
        Filter filter = new Filter()
        {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results)
            {
                if (enterpriseListTemp)
                {
                    enterpriseList = enterpriseListFull;
                    notifyDataSetChanged();
                    enterpriseListTemp = false;
                }
                else
                {
                    enterpriseList = (List<Enterprise>) results.values;
                    notifyDataSetChanged();
                }
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint)
            {
                FilterResults results = new FilterResults();
                ArrayList<Enterprise> FilteredArrayNames = new ArrayList<Enterprise>();

                constraint = constraint.toString().toLowerCase();

                if (!constraint.equals(""))
                {
                    for (int i = 0; i < enterpriseList.size(); i++)
                    {
                        Enterprise dataNames = enterpriseList.get(i);
                        if (dataNames.getEnterprise_name().toLowerCase().startsWith(constraint.toString()))
                        {
                            FilteredArrayNames.add(dataNames);
                        }
                    }

                    results.count = FilteredArrayNames.size();
                    results.values = FilteredArrayNames;
                    Log.e("VALUES", results.values.toString());
                    return results;
                }

                else
                {
                    enterpriseListTemp = true;
                    return results;
                }
            }
        };

        return filter;
    }


    //Finalidade do ViewHolder e guardar as views
    public class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout parentLayout;

        ImageView img;
        TextView txtName;
        TextView txtCountry;
        TextView txtType;

        public ViewHolder(View itemView) {
            super(itemView);

            parentLayout = itemView.findViewById(R.id.parent_layout);

            img = itemView.findViewById(R.id.imgLogo);
            txtName = itemView.findViewById(R.id.txtTitle);
            txtCountry = itemView.findViewById(R.id.txtCountry);
            txtType = itemView.findViewById(R.id.txtType);
        }
    }

}
