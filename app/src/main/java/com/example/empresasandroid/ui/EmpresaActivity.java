package com.example.empresasandroid.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.empresasandroid.R;
import com.example.empresasandroid.model.entity.ResponseEnterprise;
import com.example.empresasandroid.model.retrofit.RetrofitInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmpresaActivity extends AppCompatActivity {

    private TextView txtTitleEmpresa;
    private TextView txtDescription;
    private ImageView imgLogo;
    private ProgressBar progressBar;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empresa);

        txtDescription = findViewById(R.id.txt_description);
        txtTitleEmpresa = findViewById(R.id.txt_title_empresa);
        imgLogo = findViewById(R.id.imgLogo);
        progressBar = findViewById(R.id.progress_bar);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();
        String enterpriseName = intent.getStringExtra("enterpriseName");
        String description = intent.getStringExtra("description");
        String pathCover = intent.getStringExtra("pathCover");
        String id = intent.getStringExtra("id");

        txtTitleEmpresa.setText(enterpriseName);
        txtDescription.setText(description);

        if(pathCover != null) {
            Glide.with(EmpresaActivity.this).load(pathCover).into(imgLogo);
        } else {
            Glide.with(EmpresaActivity.this).load(R.drawable.img_e_1).into(imgLogo);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
