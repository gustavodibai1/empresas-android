package com.example.empresasandroid.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.empresasandroid.R;
import com.example.empresasandroid.model.entity.ConfigurationModel;
import com.example.empresasandroid.model.entity.Enterprise;
import com.example.empresasandroid.model.retrofit.RetrofitInstance;
import com.example.empresasandroid.ui.adapter.RecyclerAdapter;
import com.example.empresasandroid.ui.utils.SharedPreferencesHelper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    RetrofitInstance instance;
    private RecyclerView recyclerView;
    private TextView txtHome;
    private Toolbar toolbar;

    List<Enterprise> enterprises;

    Gson gson;
    RecyclerAdapter adapter;
    ConfigurationModel configurationModel;

    List<Enterprise> enterpriseList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);
        txtHome = findViewById(R.id.txtHome);
        toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        gson = new Gson();
        enterprises = new ArrayList<>();
        instance = new RetrofitInstance();
        configurationModel = new ConfigurationModel();
        adapter = new RecyclerAdapter(MainActivity.this, enterprises);

        txtHome.setVisibility(View.VISIBLE);


        try {
            Intent intent = getIntent();
            Bundle b = intent.getExtras();

            assert b != null;
            configurationModel = gson.fromJson(b.getSerializable("mConfigurationModel").toString(), ConfigurationModel.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    //Pega as empresas da API passando os 3 main-headers e da um response
    public void mostrarEmpresas(){
        SharedPreferencesHelper helper = new SharedPreferencesHelper(this);

        Call<JsonObject> call = new RetrofitInstance().getAPI().getEmpresas(
                configurationModel.getAccessToken(),
                configurationModel.getClient(),
                configurationModel.getUid()
        );

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()){
                    enterpriseList = Arrays.asList(gson.fromJson(response.body().get("enterprises"), Enterprise[].class));
                    LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
                    adapter = new RecyclerAdapter(MainActivity.this, enterpriseList);

                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(adapter);
                } else {
                    Toast.makeText(MainActivity.this, "aaaa", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchView mSearchView = (SearchView) searchItem.getActionView();
        mSearchView.setQueryHint(getString(R.string.hintSearchViewHome));


        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                txtHome.setVisibility(View.INVISIBLE);
                mostrarEmpresas();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                recyclerView.setVisibility(View.VISIBLE);
                txtHome.setVisibility(View.INVISIBLE);
                MainActivity.this.adapter.getFilter().filter(newText);
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }
}
