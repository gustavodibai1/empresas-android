package com.example.empresasandroid.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.empresasandroid.R;
import com.example.empresasandroid.model.entity.ConfigurationModel;
import com.example.empresasandroid.model.entity.User;
import com.example.empresasandroid.model.retrofit.RetrofitInstance;
import com.example.empresasandroid.ui.utils.ConnectivityHelper;
import com.example.empresasandroid.ui.utils.SharedPreferencesHelper;
import com.google.gson.Gson;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private Button btnEntrar;
    private EditText editEmail;
    private EditText editSenha;

    ConfigurationModel configurationModel;
    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnEntrar = findViewById(R.id.btnEntrar);
        editEmail = findViewById(R.id.editEmail);
        editSenha = findViewById(R.id.editSenha);

        configurationModel = new ConfigurationModel();
        gson = new Gson();

        editEmail.setText("testeapple@ioasys.com.br");
        editSenha.setText("12341234");

        login();
    }

    public void login() {
        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = editEmail.getText().toString();
                String senha = editSenha.getText().toString();

                if(ConnectivityHelper.verificaConexao(LoginActivity.this)){
                }

                if (email.isEmpty() || senha.isEmpty()) {
                    Toast.makeText(LoginActivity.this, getString(R.string.loginCamposEmpty), Toast.LENGTH_SHORT).show();
                } else if (ConnectivityHelper.verificaConexao(LoginActivity.this)) {
                    Toast.makeText(LoginActivity.this, getString(R.string.loginSemConexaoInternet), Toast.LENGTH_SHORT).show();
                } else {
                    User usuario = new User(email, senha);
                    logar(usuario);
                }
            }
        });
    }

    public void logar(User usuario) {
        RetrofitInstance instance = new RetrofitInstance();
        instance.getAPI().loginUsuario(usuario).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    configurationModel.setAccessToken(response.headers().get("access-token"));
                    configurationModel.setClient(response.headers().get("client"));
                    configurationModel.setUid(response.headers().get("uid"));

                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.putExtra("mConfigurationModel",gson.toJson(configurationModel));//Passando os parametro para as proximas requisicoes
                    startActivity(intent);

                } else {
                    Toast.makeText(LoginActivity.this, getString(R.string.loginUserPasswordIncorrect), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(LoginActivity.this, getString(R.string.loginUserPasswordIncorrect), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getHeaders(Headers headers) {
        String token = headers.get("access-token");
        String client = headers.get("client");
        String uid = headers.get("uid");

        SharedPreferencesHelper sharedPreferencesHelper = new SharedPreferencesHelper(this);
        sharedPreferencesHelper.saveToSharedPreferences(token, client, uid);
    }
}
